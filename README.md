# Fireblocks api_client

API Client script that was demonstrated in SKO 2022,
Handles deposits and withdrawal flows, deposit addresses generation watchdog and Webhook listener.
Designed to show transfers of tokens on ETH network.

## Getting started
Clone the Repo and the code and "pip install -r requirements.txt"
- Make sure to load API user id and API private key into config file

## Roadmap
Road map is to add a Front End to the project and extend the available API flows 

## License
No lisence needed you are lucky

## Guidance and support
Can reach out to Tomer La France on Slack
