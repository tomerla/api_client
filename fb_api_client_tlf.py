import base64
import time
import json
import threading
import logging
import falcon
import rsa
import base64

from wsgiref import simple_server
from config import *
from fireblocks_sdk import FireblocksSDK, TransferPeerPath, TRANSACTION_STATUS_CONFIRMED, TRANSACTION_STATUS_CANCELLED, TRANSACTION_STATUS_REJECTED, TRANSACTION_STATUS_FAILED, VAULT_ACCOUNT,\
    TRANSACTION_MINT, TRANSACTION_BURN, FireblocksApiException, DestinationTransferPeerPath, ONE_TIME_ADDRESS

privateKey = open(PRIVATE_KEY_LOCATION, 'r').read()

fireblocks = FireblocksSDK(privateKey, API_KEY)

# "DB" Mock:
btc_addresses_pool = []
eth_addresses_pool = []

REFILL_AMOUNT = 2
OMNIBUS_VAULT_ID = "13"

FIREBLOCKS_PUBLIC_KEY = """
-----BEGIN PUBLIC KEY-----
MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEA0+6wd9OJQpK60ZI7qnZG
jjQ0wNFUHfRv85Tdyek8+ahlg1Ph8uhwl4N6DZw5LwLXhNjzAbQ8LGPxt36RUZl5
YlxTru0jZNKx5lslR+H4i936A4pKBjgiMmSkVwXD9HcfKHTp70GQ812+J0Fvti/v
4nrrUpc011Wo4F6omt1QcYsi4GTI5OsEbeKQ24BtUd6Z1Nm/EP7PfPxeb4CP8KOH
clM8K7OwBUfWrip8Ptljjz9BNOZUF94iyjJ/BIzGJjyCntho64ehpUYP8UJykLVd
CGcu7sVYWnknf1ZGLuqqZQt4qt7cUUhFGielssZP9N9x7wzaAIFcT3yQ+ELDu1SZ
dE4lZsf2uMyfj58V8GDOLLE233+LRsRbJ083x+e2mW5BdAGtGgQBusFfnmv5Bxqd
HgS55hsna5725/44tvxll261TgQvjGrTxwe7e5Ia3d2Syc+e89mXQaI/+cZnylNP
SwCCvx8mOM847T0XkVRX3ZrwXtHIA25uKsPJzUtksDnAowB91j7RJkjXxJcz3Vh1
4k182UFOTPRW9jzdWNSyWQGl/vpe9oQ4c2Ly15+/toBo4YXJeDdDnZ5c/O+KKadc
IMPBpnPrH/0O97uMPuED+nI6ISGOTMLZo35xJ96gPBwyG5s2QxIkKPXIrhgcgUnk
tSM7QYNhlftT4/yVvYnk0YcCAwEAAQ==
-----END PUBLIC KEY-----
"""

signature_pub_key = rsa.PublicKey.load_pkcs1_openssl_pem(FIREBLOCKS_PUBLIC_KEY)


class RequestBodyMiddleware(object):
    def process_request(self, req, resp):
        req.body = req.bounded_stream.read()


class AuthMiddleware(object):
    def process_request(self, req, resp):
        signature = req.get_header('FIREBLOCKS-SIGNATURE')

        if signature is None:
            raise falcon.HTTPUnauthorized('Signature required')

        if not self._signature_is_valid(req.body, signature):
            raise falcon.HTTPUnauthorized('Invalid signature')

    def _signature_is_valid(self,  body, signature):
        try:
            hashing_alg = rsa.verify(body, base64.b64decode(signature), signature_pub_key)
            return hashing_alg == "SHA-512"
        except rsa.pkcs1.VerificationError:
            return False


class WebhookProcessor(object):
    def __init__(self, lock):
        self.vault_id_to_sweep_from = ""
        self.lock = lock
        self.amount_to_sweep = ""
        self.asset_id = ""
        self.fb_tx_id = ""
        self.fb_txs = []

    def on_post(self, req, resp):
        obj = json.loads(req.body.decode("utf-8"))
        print(obj)
        resp.status = falcon.HTTP_201
        if obj["type"] == "TRANSACTION_CREATED":
            self.fb_tx_id = obj["data"]["id"]
            if obj["data"]["source"]["name"] == "External":
                dep_addr = obj["data"]["destinationAddress"]
                for i in range(len(eth_addresses_pool)):
                    if eth_addresses_pool[i]["eth_addr"] == dep_addr:
                        self.vault_id_to_sweep_from = eth_addresses_pool[i]["vault_account_id"]
                        self.amount_to_sweep = str(obj["data"]["amount"])
                        self.asset_id = obj["data"]["assetId"]
                        with self.lock:
                            if eth_addresses_pool[i].get("deposited"):
                                eth_addresses_pool[i]["deposited"] += obj["data"]["amount"]
                            else:
                                eth_addresses_pool[i]["deposited"] = obj["data"]["amount"]
                            eth_addresses_pool[i]["asset_id"] = self.asset_id

        elif obj["type"] == "TRANSACTION_STATUS_UPDATED":
            self.fb_tx_id = obj["data"]["id"]
            if obj["data"]["source"]["name"] == "External":
                if obj["data"]["status"] == "COMPLETED":
                    if self.fb_tx_id not in self.fb_txs:
                        self.fb_txs.append(self.fb_tx_id)
                        is_not_gassed = True
                        while is_not_gassed:
                            gas_check_resp = fireblocks.get_vault_account(self.vault_id_to_sweep_from)
                            for asset in gas_check_resp["assets"]:
                                if asset["id"] == "ETH_TEST":
                                    if asset["balance"] != "0":
                                        is_not_gassed = False
                                        self.transfer()
                                    if is_not_gassed:
                                        print("Not enough gas yet. waiting for gas to arrive")
                                        time.sleep(5)

    def transfer(self):
        logging.info("Sweeping to Omnibus...")
        tx_result = fireblocks.create_transaction(
            asset_id=self.asset_id,
            amount=self.amount_to_sweep,
            source=TransferPeerPath("VAULT_ACCOUNT", self.vault_id_to_sweep_from),
            destination=DestinationTransferPeerPath("VAULT_ACCOUNT", OMNIBUS_VAULT_ID)
        )
        print(tx_result)


class WebhookListener:
    def __init__(self, lock):
        self.logger = logging.getLogger(f"DepositAddressesManager.{self.__class__.__name__}")
        self.logger.info("Starting Webhook listener")
        self.app = falcon.API(
            middleware=[
                RequestBodyMiddleware(),
                AuthMiddleware()
            ]
        )

        self.app.add_route('/webhook', WebhookProcessor(lock))
        self.webhook_thread = threading.Thread(target=self.webhook_starter, daemon=True)
        self.webhook_thread.start()

    def webhook_starter(self):
        httpd = simple_server.make_server('127.0.0.1', 8000, self.app)
        httpd.serve_forever()


class DepositAddressesManager:
    BTC_LOWEST_FREE_ADDRESSES_ALLOWED = 0
    ETH_LOWEST_FREE_ADDRESSES_ALLOWED = 0

    btc_free_addresses = 0
    eth_free_addresses = 0
    eth_vault_account_counter = 0

    def __init__(self):
        self.logger = logging.getLogger(self.__class__.__name__)
        # print("Creating the BTC Vault Account to create deposit addresses within..")
        self.logger.info("Creating the BTC Vault Account to create deposit addresses within..")
        self.btc_vault = fireblocks.create_vault_account(f"SKO-BTC_deposits", autoFuel=True)
        self.btc_vault_account_id = self.btc_vault["id"]
        self.btc_main_addr = fireblocks.create_vault_asset(self.btc_vault["id"], "BTC_TEST")
        self.lock = threading.Lock()
        self.webhook = WebhookListener(self.lock)
        self.watchdog_thread = threading.Thread(target=self.available_addresses_watchdog, daemon=True)
        self.watchdog_thread.start()
        self.addresses_allocator()

    def available_addresses_watchdog(self):
        while True:
            if self.btc_free_addresses <= self.BTC_LOWEST_FREE_ADDRESSES_ALLOWED or \
                    self.eth_free_addresses <= self.ETH_LOWEST_FREE_ADDRESSES_ALLOWED:
                self.addresses_generator()
            time.sleep(10)

    def addresses_generator(self):
        with self.lock:
            if self.btc_free_addresses <= self.BTC_LOWEST_FREE_ADDRESSES_ALLOWED:
                # print("Generating BTC deposit addresses to have in advance..")
                self.logger.info("Generating BTC deposit addresses to have in advance..")
                for i in range(REFILL_AMOUNT):
                    btc_addr = fireblocks.generate_new_address(self.btc_vault["id"], "BTC_TEST")
                    btc_addresses_pool.append({
                        "btc_addr": btc_addr["address"],
                        "allocated": "no",
                        "customer_name": ""
                    })
                    self.btc_free_addresses += 1

            if self.eth_free_addresses <= self.ETH_LOWEST_FREE_ADDRESSES_ALLOWED:
                # print("Generating ETH deposit addresses to have in advance..")
                self.logger.info("Generating ETH deposit addresses to have in advance..")
                for i in range(REFILL_AMOUNT):
                    eth_vault = \
                        fireblocks.create_vault_account(f"SKO-ETH_deposits_#{self.eth_vault_account_counter}", autoFuel=True)
                    eth_addr = fireblocks.create_vault_asset(eth_vault["id"], "ETH_TEST")
                    eth_addresses_pool.append({
                        "eth_addr": eth_addr["address"],
                        "vault_account_id": eth_vault["id"],
                        "allocated": "no",
                        "customer_name": ""
                    })
                    self.eth_free_addresses += 1
                    self.eth_vault_account_counter += 1
            # print(btc_addresses_pool)
            self.logger.info(btc_addresses_pool)
            # print(eth_addresses_pool)
            self.logger.info(eth_addresses_pool)

    def addresses_allocator(self):
        asset = input("For deposit address pls enter BTC or ETH. For Withdrawal enter withdrawal. "
                      "enter 'balance' for addresses balance prompt or 'stop' for finish:____")
        while asset != 'stop':
            with self.lock:
                if asset == "ETH":
                    deposit_address = None
                    for eth in eth_addresses_pool:
                        if eth["allocated"] == "no":
                            eth["allocated"] = "yes"
                            deposit_address = eth["eth_addr"]
                            eth["customer_name"] = input("Enter customer name (/leave blank): ")
                            self.eth_free_addresses -= 1
                            break
                    if deposit_address:
                        print(f"Your deposit address is: {deposit_address}")
                    else:
                        print("ETH Deposit addresses pool is out of addresses. "
                              "You should consider creating larger pools")

                elif asset == "BTC":
                    deposit_address = None
                    for btc in btc_addresses_pool:
                        if btc["allocated"] == "no":
                            btc["allocated"] = "yes"
                            deposit_address = btc["btc_addr"]
                            btc["customer_name"] = input("Enter customer name (/leave blank): ")
                            self.btc_free_addresses -= 1
                            break
                    if deposit_address:
                        print(f"Your deposit address is: {deposit_address}")
                    else:
                        print("BTC Deposit addresses pool is out of addresses. "
                              "You should consider creating larger pools")

                elif asset == "balance":
                    print(f"BTC free addresses: {self.btc_free_addresses}")
                    print(f"ETH free addresses: {self.eth_free_addresses}")
                    print(f"btc_addresses_pool: {btc_addresses_pool}")
                    print(f"eth_addresses_pool: {eth_addresses_pool}")

                elif asset == "withdrawal":
                    is_transfer_done = False
                    customer_name = input("Enter customer name: ")
                    for eth in eth_addresses_pool:
                        if eth["customer_name"] == customer_name:
                            if eth.get("deposited"):
                                withdrawal_amount = str(eth["deposited"] - 1)
                                withdrawal_address = input("Enter address for withdrawal: ")
                                tx_result = fireblocks.create_transaction(
                                    asset_id=eth["asset_id"],
                                    amount=withdrawal_amount,
                                    source=TransferPeerPath("VAULT_ACCOUNT", OMNIBUS_VAULT_ID),
                                    destination=DestinationTransferPeerPath(ONE_TIME_ADDRESS, one_time_address={"address": withdrawal_address})
                                )
                                print(tx_result)
                                if tx_result:
                                    if tx_result.get("status") == "SUBMITTED":
                                        is_transfer_done = True
                                        eth_addresses_pool[eth_addresses_pool.index(eth)]["deposited"] = 0
                                        self.logger.info(f"Withdrawal transfer has been submitted. Deposited balance is now 0")
                    if not is_transfer_done:
                        self.logger.error("Customer name was not found")

            asset = input("For deposit address pls enter BTC or ETH. For Withdrawal enter withdrawal. "
                          "enter 'balance' for addresses balance prompt or 'stop' for finish:____")
        print("bye")


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format="%(asctime)s [%(levelname)s] %(message)s")
    play = DepositAddressesManager()


